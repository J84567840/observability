package com.grcp.demo.observability;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class DemoController {

    private final RestTemplate restTemplate;

    public DemoController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping()
    public ResponseEntity<HelloResponse> home(
            @RequestHeader(value = "customer-id", required = false) String customerId) {
        System.out.printf(">>>> customerId: %s%n", customerId);
        return restTemplate.getForEntity("http://localhost:8080/hello", HelloResponse.class);
    }

    @GetMapping("/hello")
    public ResponseEntity<HelloResponse> hello(
            @RequestHeader(value = "customer-id", required = false) String customerId) {
        return ResponseEntity.ok(new HelloResponse(customerId));
    }

    public record HelloResponse(String value) {}
}
