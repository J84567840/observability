package com.grcp.demo.observability.logging;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.owasp.encoder.Encode;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Thomas Sutton on 19/07/2022.
 *
 * Intercepts the incoming request and extracts headers that could have been set by API Gateway etc and sets them into the
 * Baggage via the BaggageHandler.
 */
@Configuration
@Order(Ordered.LOWEST_PRECEDENCE - 2)
public class BaggageManagementFilter implements Filter {

    private final BaggageHandler baggageHandler;

    @Autowired
    public BaggageManagementFilter(final BaggageHandler baggageHandler) {
        this.baggageHandler = baggageHandler;
    }

    @Override
    public void init(final FilterConfig filterConfig) {
        //NO-OP
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        try {
            boolean doFilter = true;
            if (((HttpServletRequest) request).getRequestURI().startsWith("/v1/")) {
                doFilter = Arrays.stream(Baggage.values())
                        .filter(baggage -> baggage.getHeaderName() != null)
                        .map(baggage -> processBaggage(baggage, (HttpServletRequest) request, (HttpServletResponse) response))
                        .allMatch(b -> b);
            }
            if (doFilter) {
                chain.doFilter(request, response);
            }
        } finally {
            MDC.clear();
        }
    }

    private boolean processBaggage(final Baggage baggage, final HttpServletRequest request, final HttpServletResponse response) {
        if (baggageHandler.getBaggageItem(baggage.getHeaderName()).isEmpty()) {
            List<String> headers = new ArrayList<>(baggage.getAltHeaderNames());
            if (headers.isEmpty()) {
                headers.add(baggage.getHeaderName());
            } else {
                headers.add(0, baggage.getHeaderName());
            }
            var headerValue = headers.stream()
                    .filter(s -> request.getHeader(s) != null)
                    .map(request::getHeader)
                    .findFirst();
            if (baggage.isMandatory() && headerValue.isEmpty()) {
                try {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST, Encode.forHtml("Mandatory header: " + baggage.getHeaderName() + " not sent in request."));
                } catch (IOException ex) {
                    throw new IllegalArgumentException("Mandatory header: " + baggage.getHeaderName() + " not sent in request.", ex);
                }
                return false;
            } else {
                baggageHandler.setBaggageItem(baggage.getHeaderName(), headerValue.orElse(baggage.getDefaultValue()));
            }
        }
        return true;
    }
}
