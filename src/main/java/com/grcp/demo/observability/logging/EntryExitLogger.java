package com.grcp.demo.observability.logging;

import ch.qos.logback.classic.Logger;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.logstash.logback.argument.StructuredArguments;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;

import java.io.IOException;

/**
 * Created by Thomas on 19/07/2022.
 * Anything that is not an actuator request will have the entry and exit from the service logged along with the request duration.
 * This is useful as we currently do not have a tracing system on the Waitrose Digital Platform. substitutions can be added
 * here is required.
 */
@WebFilter(urlPatterns = {})
@Order(Ordered.LOWEST_PRECEDENCE - 1)
public class EntryExitLogger implements Filter {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(EntryExitLogger.class);

    private final BaggageHandler baggageHandler;

    private final String appName;

    @Autowired
    public EntryExitLogger(final BaggageHandler baggageHandler,
                           @Value("${spring.application.name}") final String appName) {
        super();
        this.baggageHandler = baggageHandler;
        this.appName = appName;
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        // empty
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        long startTime = System.currentTimeMillis();
        try {
            entryLogging(request);
            chain.doFilter(request, response);
        } finally {
            exitLogging(request, (HttpServletResponse) response, startTime);
        }
    }

    private void entryLogging(final ServletRequest request) {
        if (!isActuatorRequest(request)) {
            logger.info(appName + " service handling request {}",
                    StructuredArguments.keyValue("logType", "RESTENTRY"));
        }
    }

    private void exitLogging(final ServletRequest request, final HttpServletResponse response, final long startTime) {
        if (isActuatorRequest(request)) {
            logger.debug(appName + " actuator request completed");
        } else {
            HttpStatus status = HttpStatus.resolve(response.getStatus());
            logger.info(appName + " service request completed {} {} {} {}",
                    StructuredArguments.keyValue("duration", System.currentTimeMillis() - startTime),
                    StructuredArguments.keyValue("httpStatus", (status != null) ? status.toString() : null),
                    StructuredArguments.keyValue("action", baggageHandler.getBaggageItem(Baggage.OPERATION.getHeaderName()).orElse("unknownOperation")),
                    StructuredArguments.keyValue("logType", "RESTEXIT"));
        }
    }

    private boolean isActuatorRequest(final ServletRequest request) {
        return request instanceof HttpServletRequest && ((HttpServletRequest) request).getRequestURI().startsWith("/actuator/");
    }

    @Override
    public void destroy() {
        // empty
    }
}

