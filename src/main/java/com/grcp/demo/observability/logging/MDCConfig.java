package com.grcp.demo.observability.logging;

import brave.baggage.BaggageField;
import brave.baggage.CorrelationScopeConfig;
import brave.context.slf4j.MDCScopeDecorator;
import brave.propagation.CurrentTraceContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This ensures that baggage is flushed out into the logs. NB You will want to update logback-spring.xml to rename fields
 * to conform to the Waitrose Logging Standards.
 */
@Configuration
public class MDCConfig {

    private final Collection<BaggageField> baggageFields;

    public MDCConfig(final Collection<BaggageField> baggageFields) {
        this.baggageFields = new ArrayList<>(baggageFields);
    }

    @Bean
    public CurrentTraceContext.ScopeDecorator mdcScopeDecorator() {
        var baseConfig = MDCScopeDecorator.newBuilder()
                .clear();
        baggageFields.stream()
                .filter(baggageField -> Baggage.findByHeaderName(baggageField.name()).isLog())
                .forEach(baggageField -> baseConfig
                        .add(CorrelationScopeConfig.SingleCorrelationField.newBuilder(baggageField)
                                .flushOnUpdate()
                                .build()));
        return baseConfig.build();
    }
}
