package com.grcp.demo.observability.logging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Items of baggage that we want to support. Add new items here. Put think carefully before adding as they do come
 * with some level of overhead and should not be used to hack global state into the service. To add a new item create an
 * entry here and also update the application.properties file to add the baggage name to either spring.sleuth.baggage.remote-field
 * spring.sleuth.baggage.local-fields depending on if you want to pass the field on to other services (remote) or just keep it local
 * to the application (local).
 */
public enum Baggage {

    CORRELATION_ID("wtr-correlation-id", Collections.singletonList("gatewayGeneratedTraceId"), true, true, true, null),
    CUSTOMER_ID("customer-id", Collections.emptyList(), false, true, true, "-1"),
    OPERATION( "operation", new ArrayList<>(), false, false, false, null);

    private final String headerName;

    private final List<String> altHeaderNames;
    private final boolean mandatory;
    private final boolean propagate;
    private final boolean log;

    private final String defaultValue;

    Baggage(final String headerName,
            final List<String> altHeaderNames,
            final boolean mandatory,
            final boolean propagate,
            final boolean log,
            final String defaultValue) {
        this.headerName = headerName;
        this.altHeaderNames = altHeaderNames;
        this.mandatory = mandatory;
        this.propagate = propagate;
        this.log = log;
        this.defaultValue = defaultValue;
    }

    public String getHeaderName() {
        return headerName;
    }

    public List<String> getAltHeaderNames() {
        return new ArrayList<>(altHeaderNames);
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public boolean isPropagate() {
        return propagate;
    }

    public boolean isLog() {
        return log;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public static Baggage findByHeaderName(final String name) {
        return Arrays.stream(Baggage.values())
                .filter(baggage -> name.equals(baggage.getHeaderName()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Baggage with the given name: " + name + " not found"));
    }
}
