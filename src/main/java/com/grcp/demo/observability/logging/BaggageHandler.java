package com.grcp.demo.observability.logging;

import java.util.Optional;

public interface BaggageHandler {

    void setBaggageItem(final String key, final String value);

    Optional<String> getBaggageItem(final String key);
}
