package com.grcp.demo.observability.logging;

import brave.baggage.BaggageField;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

/**
 * Implementation of the BaggageHandler using Spring Cloud Sleuth.
 */
@Service
public final class SleuthBaggageHandler implements BaggageHandler {

    private final Collection<BaggageField> baggageFields;

    public SleuthBaggageHandler(final Collection<BaggageField> baggageFields) {
        this.baggageFields = new ArrayList<>(baggageFields);
    }

    @Override
    public void setBaggageItem(final String key, final String value) {
        baggageFields.stream().filter(bf -> key.equals(bf.name()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Can not set baggage item: " + key + " as key name is not permitted." +
                                                                " Check BaggageFields class to and add your key if required."))
                .updateValue(value);
    }

    @Override
    public Optional<String> getBaggageItem(final String key) {
        return Optional.ofNullable(baggageFields.stream().filter(bf -> key.equals(bf.name()))
                        .findFirst()
                        .orElseThrow(() -> new IllegalArgumentException("Can not get baggage item: " + key + " as key name is not permitted." +
                                                                        " Check BaggageFields class to and add your key if required.")))
                .map(BaggageField::getValue);
    }

}
