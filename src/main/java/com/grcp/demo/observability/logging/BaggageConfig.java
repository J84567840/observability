package com.grcp.demo.observability.logging;

import brave.baggage.BaggageField;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class BaggageConfig {

    @Bean
    List<BaggageField> setupBaggage() {
        return Arrays.stream(Baggage.values())
                .map(baggage -> BaggageField.create(baggage.getHeaderName()))
                .toList();
    }
}

