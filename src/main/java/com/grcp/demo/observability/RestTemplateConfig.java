package com.grcp.demo.observability;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                // Test code to print out all headers - you don't need this code
                .additionalInterceptors((request, body, execution) -> {
                    request.getHeaders().forEach((s, strings) -> System.out.printf("HEADER [%s] VALUE %s%n", s, strings));
                    return execution.execute(request, body);
                })
                .build();
    }
}
